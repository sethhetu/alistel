// Copyright(c) 2016, Seth N.Hetu
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met :
//
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and / or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>

//Note: If you are getting include errors, you still need to download the Steamworks SDK.
//      Download the SDK (i.e., steamworks_sdk_138a.zip), and extract it into the Alistel "steamworks" folder.
//      Your main folder should look like "Alistel/steamworks/sdk/public", etc.
#include "../steamworks/sdk/public/steam/steam_api.h"


//This class holds everything about Steam, and is used as a broker with the Steam API from within the scripting system.
class MySteamSuperClass {
public:
	MySteamSuperClass() {
	}

	//Initialize.
	int init() {
		//Try to open our logfile.
		if (!log_file.is_open()) {
			log_file.open("steam_log.txt");
		}

		//Try to initialize steam.
		if (SteamAPI_Init()) {
			get_logfile() << "Steam API initialized." << std::endl;
		}
		else {
			get_logfile() << "Steam API failed to initialize for some reason..." << std::endl;
			return 1;
		}

		return 0;
	}

	int shutdown() {
		//Now, shut down steam.
		SteamAPI_Shutdown();
		get_logfile() << "Steam API shut down." << std::endl;

		//Now, reset all our internal state.
		log_file.close();

		return 0;
	}

	void disableScreenshots() {
		//Try to over-ride the screenshot key.
		ISteamScreenshots* ss = SteamScreenshots();
		if (ss) { ss->HookScreenshots(true); }
	}


	//Get our output stream (file).
	std::ostream& get_logfile() const {
		if (log_file.is_open()) {
			return log_file;
		}
		return std::cout; //Umm... better than crashing?
	}

	int getNumEarnedAchieves() const {
		ISteamUser* user = SteamUser();
		ISteamUserStats* userStats = SteamUserStats();
		ISteamFriends* userFriends = SteamFriends();
		if (!(user && userStats)) {
			get_logfile() << "Can't get Steam achievements; user or user_stats is null." << std::endl;
			return 0;
		}
		get_logfile() << "Requesting stats for Steam player: " << std::string(userFriends ? userFriends->GetPersonaName() : "<unknown>") << std::endl;
		if (!userStats->RequestCurrentStats()) {
			get_logfile() << "Can't get Steam achievements; unknown error." << std::endl;
			return 0;
		}

		//Count the current achievements
		unsigned int numAchievesTotal = userStats->GetNumAchievements();
		get_logfile() << "Steam server lists " << numAchievesTotal << " total achievements." << std::endl;

		//Iterate, and check stats as we go.
		int res = 0;
		for (unsigned int i = 0; i < numAchievesTotal; i++) {
			const char* achieve = userStats->GetAchievementName(i); //Let's hope this is managed by Steam; the API is unclear.
			if (!achieve) {
				get_logfile() << "Achievement id " << i << " is invalid." << std::endl;
				continue;
			}
			bool gotIt = false;
			if (!userStats->GetAchievement(achieve, &gotIt)) {
				get_logfile() << "Achievement id " << i << "; couldn't get stats." << std::endl;
				continue;
			}
			if (gotIt) {
				res += 1;
			}
		}

		get_logfile() << "User has earned " << res << " achievements." << std::endl;
		return res;
	}

	//Achievements are pretty simple (compared to leaderboards). Returns 0 for ok, 1 for error.
	int syncAchievement(std::string achievementName) {
		//Can't do achievements if steam is not loaded.
		ISteamUser* user = SteamUser();
		ISteamUserStats* userStats = SteamUserStats();
		ISteamFriends* userFriends = SteamFriends();
		if (!(user && userStats)) {
			get_logfile() << "Can't get Steam achievements; user or user_stats is null." << std::endl;
			return 1;
		}
		get_logfile() << "Requesting stats for Steam player: " << std::string(userFriends ? userFriends->GetPersonaName() : "<unknown>") << std::endl;
		if (!userStats->RequestCurrentStats()) {
			get_logfile() << "Can't get Steam achievements; unknown error." << std::endl;
			return 1;
		}

		//Print the current achievements
		get_logfile() << "Steam server lists " << userStats->GetNumAchievements() << " total achievements." << std::endl;

		//The only thing we refuse to sync is an empty string. Go wild with crazy SQL-injection achievements (please don't).
		if (!achievementName.empty()) {
			bool status = false;
			if (!userStats->GetAchievement(achievementName.c_str(), &status)) {
				get_logfile() << "Can't get Steam achievement \"" << achievementName << "; unknown error." << std::endl;
				return 1;
			}

			//Only update achievements steam doesn't know we've earned yet.
			if (!status) {
				get_logfile() << "Registering achievement: " << achievementName << std::endl;
				userStats->SetAchievement(achievementName.c_str());
			} else {
				get_logfile() << "Achievement has already been earned by Steam: " << achievementName << std::endl;
			}
		}

		//Update the server.
		userStats->StoreStats();

		return 0;
	}


private:
	//Logfile for all actions. Created on startup.
	mutable std::ofstream log_file;
};



//Get our Steam singleton instance.
MySteamSuperClass& get_steam() {
	static MySteamSuperClass my_steam_super;
	return my_steam_super;
}


//
//Sample usage, in Ruby.
//
//Somewhere global, set up your function pointers:
//  @@fnSteamInit = Win32API.new("Alistel.dll", "steam_init", "v", "i")
//  @@fnSteamShutdown = Win32API.new("Alistel.dll", "steam_shutdown", "v", "i")
//  @@fnSteamAchievement = Win32API.new("Alistel.dll", "steam_sync_achievement", "p", "i")
//  @@fnSteamNumEarnedAchieves = Win32API.new("Alistel.dll", "steam_get_num_earned_achieves", "v", "i")
//  @@fnSteamDisableScreenshots = Win32API.new("Alistel.dll", "steam_disable_screenshots", "v", "i")
//
//Next, initialize the API once (at the start of your game):
//  @@fnSteamInit.call()
//
//Assume you have a few achievements:
//  Achievements = [
//    #SteamName,            Switch
//    ["MyAchievement",      3000],
//    ["MyOtherAchievement", 3001],
//  ]
//
//...and some way of tracking which ones you've synced successfully:
//  @already_earned = {3000=>false, 3001=>false}
//
//Then, when you want to sync achievements:
//  Achievements.each{|ach|
//    next if @already_earned[ach[1]]  #You can override this if you need a "force sync" option, but try not to spam the Steam API
//    if $game_switches[ach[1]]
//      @@fnSteamAchievement.call(ach[0])
//    end
//  }
//
//@@fnSteamNumEarnedAchieves/@@fnSteamDisableScreenshots are sometimes useful, and @@fnSteamShutdown should be called when your game exits (but is not strictly mandatory).



extern "C" {
	//Must be called first.
	__declspec(dllexport)
		int steam_init()
	{
		return get_steam().init();
	}

	//Unlikely you will need this, but it *does* reset the state of the system in case you want to try again.
	__declspec(dllexport)
		int steam_shutdown()
	{
		return get_steam().shutdown();
	}

	//Call this to sync a single achievement. Note that you do NOT need to call steam_tick().
	//Note that a return value of 1 does *not* disable everything (like leaderboards). 
	//Note that this should still work even if leaderboards are messed up for some reason.
	__declspec(dllexport)
		int steam_sync_achievement(const char* achievementName)
	{
		return get_steam().syncAchievement(achievementName);
	}

	//Call this to return the number of achievements this user account has achieved (in total, as far as steam is concerned).
	//Returns 0 on error.
	__declspec(dllexport)
		int steam_get_num_earned_achieves()
	{
		return get_steam().getNumEarnedAchieves();
	}

	//Call if your application uses F12 to do something else (like reset the game) and you don't want this to trigger the screenshot dialog box every time.
	__declspec(dllexport)
		int steam_disable_screenshots()
	{
		get_steam().disableScreenshots();
		return 1;
	}
}


