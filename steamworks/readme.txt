The Steamworks SDK should go here. 
Download it from the Steamworks partner page:
   https://partner.steamgames.com/home
...and unzip it here. You should end up with a folder titled "sdk" in the same folder as this readme.
